#pragma once

#include "Eigen/Dense"
#include <ros/console.h>
#include <rosdyn_core/primitives.h>
#include <rosdyn_core/frame_distance.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/JointState.h>
#include <eigen_conversions/eigen_msg.h>


namespace obstacleHandler
{

class pointObstacleArray 
{
protected:
  double m_st;
  std::basic_string<char> m_frame_id;

  std::vector<std::string> m_test_links;

  std::vector<Eigen::Affine3d,Eigen::aligned_allocator<Eigen::Affine3d>> m_obstacle_pose_vector;
  unsigned int m_closest_obstacle_id;
//  std::vector<Eigen::Affine3d> m_obstacle_pose_vector_old; // to compute velocity
//  unsigned int m_closest_obstacle_old_id;
  unsigned int m_filter_counter;
  unsigned int m_max_lost_samples;
  Eigen::VectorXd m_joint_config;
  rosdyn::ChainPtr m_chain;

public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  pointObstacleArray();
  void setSamplingPeriod(const double& st);
  double getSamplingPeriod();
  void setNumberAxis(const unsigned int& nax);
  void setBaseFrame(std::basic_string<char>& frame_id);
  void setTestLinks(const std::vector<std::string>& test_links){m_test_links=test_links;}
  std::basic_string<char> getBaseFrame();
  void setChain(const rosdyn::ChainPtr& chain);
  double computeClosest(Eigen::Affine3d& closest_obstacle );
  std::vector<Eigen::Affine3d,Eigen::aligned_allocator<Eigen::Affine3d>> getArray();
  void setMaxLostSamples(const unsigned int& max_lost_samples );
  void setArrayFromMsg(const geometry_msgs::PoseArrayConstPtr& msg);
  void jointStateCallback(const sensor_msgs::JointStateConstPtr& msg);

};

}

