#include <obstacle_handler/obstacle_handler.h>


namespace obstacleHandler
{

pointObstacleArray::pointObstacleArray()
{
  m_obstacle_pose_vector.resize(1);
  m_obstacle_pose_vector.at(0).translation()(0)=1.0e3;
  m_obstacle_pose_vector.at(0).translation()(1)=1.0e3;
  m_obstacle_pose_vector.at(0).translation()(2)=1.0e3;
  m_filter_counter=0;
}

void pointObstacleArray::setSamplingPeriod(const double& st)
{
  if (st>0.0)
    m_st=st;
}

double pointObstacleArray::getSamplingPeriod()
{
  return m_st;
}

void pointObstacleArray::setNumberAxis(const unsigned int& nax)
{
  m_joint_config.resize(nax);
}


void pointObstacleArray::setBaseFrame(std::basic_string<char>& frame_id)
{
  m_frame_id=frame_id;
}

std::basic_string<char> pointObstacleArray::getBaseFrame()
{
  return m_frame_id;
}

void pointObstacleArray::setChain(const rosdyn::ChainPtr& chain)
{
  m_chain=chain;
}


double pointObstacleArray::computeClosest(Eigen::Affine3d& closest_obstacle )
{
  std::vector<Eigen::Affine3d,Eigen::aligned_allocator<Eigen::Affine3d>> robotPoints;
  robotPoints=m_chain->getTransformations(m_joint_config);
  std::vector<std::string> link_names=m_chain->getLinksName();

  double min_distance=1.0e3;//std::numeric_limits<double>::infinity();
  unsigned int obstacle_array_size=m_obstacle_pose_vector.size();
  m_closest_obstacle_id=0;
  for (unsigned int i_link=0;i_link<robotPoints.size();i_link++)
  {
    if (std::find(m_test_links.begin(),m_test_links.end(),link_names.at(i_link))==m_test_links.end())
        continue;
    for (unsigned int i_obs=0;i_obs<obstacle_array_size;i_obs++)
    {
      double distance=(m_obstacle_pose_vector.at(i_obs).translation()-robotPoints.at(i_link).translation()).norm();
      if (distance<=min_distance)
      {
        m_closest_obstacle_id=i_obs;
        min_distance=distance;
      }
    }
  }
  closest_obstacle=m_obstacle_pose_vector.at(m_closest_obstacle_id);
  return min_distance;
}

std::vector<Eigen::Affine3d, Eigen::aligned_allocator<Eigen::Affine3d> > pointObstacleArray::getArray()
{
  return m_obstacle_pose_vector;
}

void pointObstacleArray::setMaxLostSamples(const unsigned int& max_lost_samples )
{
  m_max_lost_samples=max_lost_samples;
}


void pointObstacleArray::setArrayFromMsg(const geometry_msgs::PoseArrayConstPtr& msg)
{
  if (m_frame_id.compare(msg->header.frame_id))
    ROS_ERROR("Obstacle frame expected to be '%s' but received msg frame is '%s'. Message neglected.",m_frame_id.c_str(),(msg->header.frame_id).c_str());
  else if (msg->poses.size()>0)
  {
    m_filter_counter=0;
    m_obstacle_pose_vector.clear();
    for (unsigned int idx=0;idx<msg->poses.size();idx++)
    {
      Eigen::Affine3d obstacleEigen;
      tf::poseMsgToEigen(msg->poses.at(idx),obstacleEigen);
      m_obstacle_pose_vector.push_back(obstacleEigen);
    }
  }
  else
  {
    m_filter_counter++;
    if (m_filter_counter>m_max_lost_samples)
    {
      m_obstacle_pose_vector.resize(1);
      m_obstacle_pose_vector.at(0).translation()(0)=1.0e3;
      m_obstacle_pose_vector.at(0).translation()(1)=1.0e3;
      m_obstacle_pose_vector.at(0).translation()(2)=1.0e3;
      m_filter_counter=m_max_lost_samples+1;
    }
  }
}

void pointObstacleArray::jointStateCallback(const sensor_msgs::JointStateConstPtr& msg)
{
  for (unsigned int inax=0;inax<m_joint_config.size();inax++)
    m_joint_config(inax)=msg->position.at(inax);
}


}
