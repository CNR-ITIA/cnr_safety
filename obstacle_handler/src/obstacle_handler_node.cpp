#include <obstacle_handler/obstacle_handler.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int64.h>
#include <geometry_msgs/PoseArray.h>
#include <sensor_msgs/JointState.h>
#include <subscription_notifier/subscription_notifier.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "obstacle_handler_node");
  ros::NodeHandle node_handle;

  obstacleHandler::pointObstacleArray obstacle_array;
  std::basic_string<char> frame_id("world");
  obstacle_array.setBaseFrame(frame_id);
  obstacle_array.setMaxLostSamples(10);

  urdf::Model model;
  rosdyn::ChainPtr chain_ee;

  std::string base_frame;
  if (!node_handle.getParam("/base_frame", base_frame))
  {
    ROS_ERROR("base_frame not defined");
    return false;
  }
  std::string tool_frame;
  if (!node_handle.getParam("/tool_frame", tool_frame))
  {
    ROS_ERROR("tool_frame not defined");
    return false;
  }
  std::vector<std::string> joint_names;
  if (!node_handle.getParam("/controlled_joints",joint_names))
  {
    ROS_ERROR("controlled_joints not defined");
    return false;
  }

  std::vector<std::string> test_links;
  if (!node_handle.getParam("/test_links",test_links))
  {
    ROS_ERROR("test_links not defined");
    return false;
  }


  std::string robot_description;
  if (!node_handle.getParam("/robot_description", robot_description))
  {
    ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
    return false;
  }
  model.initParam("robot_description");
  Eigen::Vector3d grav;
  grav << 0, 0, -9.806;
  chain_ee = rosdyn::createChain(model,base_frame,tool_frame,grav);
  chain_ee->setInputJointsName(joint_names);

  unsigned int nax=chain_ee->getActiveJointsNumber();
  obstacle_array.setNumberAxis(nax);
  obstacle_array.setChain(chain_ee);
  obstacle_array.setTestLinks(test_links);

  bool publish_ovr=false;
  if (!node_handle.getParam("publish_ovr",publish_ovr))
  {
    publish_ovr=false;
  }
  ros::Publisher min_distance_pub=node_handle.advertise<std_msgs::Float64>("/distance",1);
  ros::Publisher closest_obstacle_pose_pub=node_handle.advertise<geometry_msgs::Pose>("/closest_obstacle",1);

  ros::Publisher ovr_pub;
  if (publish_ovr)
  {
    ovr_pub=node_handle.advertise<std_msgs::Int64>("/safe_ovr_1",1);
  }

  auto obstacle_cb=boost::bind(&obstacleHandler::pointObstacleArray::setArrayFromMsg,&obstacle_array,_1);
  ros_helper::SubscriptionNotifier<geometry_msgs::PoseArray> obstacle_notif(node_handle,"/poses",1,obstacle_cb);
  if (!obstacle_notif.waitForANewData())
  {
    ROS_ERROR("timeout: no new messages from topic /poses");
    return 0;
  }
  auto js_cb=boost::bind(&obstacleHandler::pointObstacleArray::jointStateCallback,&obstacle_array,_1);
  ros_helper::SubscriptionNotifier<sensor_msgs::JointState> js_notif(node_handle,"/joint_states",1,js_cb);
  if (!js_notif.waitForANewData())
  {
    ROS_ERROR("timeout: no new messages from topic /joint_states");
    return 0;
  }

  //  ros::spinOnce();

//  ros::Duration(10.0).sleep();
  ros::Rate r(100);
  while (ros::ok())
  {
    obstacle_notif.isANewDataAvailable();

    Eigen::Affine3d closest_obstacle;
    double min_distance = obstacle_array.computeClosest(closest_obstacle);
    std_msgs::Float64Ptr min_distance_msg(new std_msgs::Float64());
    min_distance_msg->data=min_distance;
    min_distance_pub.publish(min_distance_msg);
    geometry_msgs::PosePtr closest_obstacle_msg(new geometry_msgs::Pose());
    closest_obstacle_msg->position.x=closest_obstacle.translation()(0);
    closest_obstacle_msg->position.y=closest_obstacle.translation()(1);
    closest_obstacle_msg->position.z=closest_obstacle.translation()(2);
    closest_obstacle_pose_pub.publish(closest_obstacle_msg);
    if (publish_ovr)
    {
      std_msgs::Int64 ovr;
      if (min_distance<0.2)
        ovr.data=0;
      else if (min_distance<1.4)
        ovr.data=(min_distance-0.2)/1.2*100.0;
      else
        ovr.data=100;

      ovr_pub.publish(ovr);
    }
    ros::spinOnce();
    r.sleep();
  }

  
  ros::shutdown();
  return 0;
}


