cmake_minimum_required(VERSION 2.8.3)
project(safety_supervisor)
add_compile_options(-std=c++11 -funroll-loops -Wall -Ofast)
set(CMAKE_BUILD_TYPE Release)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  subscription_notifier
  configuration_msgs
  std_msgs
  moveit_core
  moveit_ros_planning_interface
)
find_package(Eigen3 REQUIRED COMPONENTS)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES safety_supervisor
  CATKIN_DEPENDS roscpp subscription_notifier configuration_msgs std_msgs moveit_core moveit_ros_planning_interface
#  DEPENDS system_lib
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}/acceleration_supervisor.cpp
  # src/${PROJECT_NAME}/collabot_supervisor.cpp
)

add_executable(collabot_supervisor src/${PROJECT_NAME}/collabot_supervisor.cpp)
target_link_libraries(collabot_supervisor
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)


add_executable(collision_monitor src/collision_monitor.cpp)
target_link_libraries(collision_monitor
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)


add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
install(TARGETS ${PROJECT_NAME} 
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h"
  PATTERN ".svn" EXCLUDE
)
