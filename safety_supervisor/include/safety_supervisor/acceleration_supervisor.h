#ifndef acceleration_supervisor_20201958
#define acceleration_supervisor_20201958

#include <safety_supervisor/safety_supervisor.h>
#include <ros/ros.h>
#include <urdf_model/model.h>
#include <urdf_parser/urdf_parser.h>
#include <Eigen/Dense>
#include <std_msgs/Float64.h>
#include <subscription_notifier/subscription_notifier.h>


namespace safety {

  class AccelerationSupervisor {

  public:
    AccelerationSupervisor();
    ~AccelerationSupervisor();

    //The method getLimitedAcc() returns the saturated joint acceleration value for n joints.
    //Same for getLimitedVel() and getLimitedPos.
    void init (std::vector<std::string>& joint_names);
    Eigen::VectorXd getBreakingAcc(Eigen::VectorXd x, Eigen::VectorXd Dx, Eigen::VectorXd DDx);
    Eigen::VectorXd getLimitedAcc(Eigen::VectorXd DDx);
    Eigen::VectorXd getLimitedVel(Eigen::VectorXd Dx);
    Eigen::VectorXd getLimitedPos(Eigen::VectorXd x);

    void velOverrideCallback(const std_msgs::Float64ConstPtr& msg);
    void accOverrideCallback(const std_msgs::Float64ConstPtr& msg);

  protected:
      ros::NodeHandle m_nh;
      urdf::ModelInterfaceSharedPtr m_robot_model;
      Eigen::VectorXd m_acceleration_limits;
      Eigen::VectorXd m_velocity_limits;
      Eigen::VectorXd m_effort_limits;
      Eigen::VectorXd m_upper_limits;
      Eigen::VectorXd m_lower_limits;
      std::vector<std::string> m_joint_names;

      unsigned int m_nAx;

      Eigen::VectorXd m_x;
      Eigen::VectorXd m_Dx;
      Eigen::VectorXd m_Dx_sat;
      Eigen::VectorXd m_DDx;
      Eigen::VectorXd m_DDx_sat;

      
      std_msgs::Float64 m_msg_vel_ovr;
      std_msgs::Float64 m_msg_acc_ovr;

      double m_vel_ratio;
      double m_acc_ratio;
      bool m_is_initialized;

      std::shared_ptr<ros_helper::SubscriptionNotifier<std_msgs::Float64>> m_sub_vel_ovr;
      std::shared_ptr<ros_helper::SubscriptionNotifier<std_msgs::Float64>> m_sub_acc_ovr;
      

            



  };                              
}


#endif