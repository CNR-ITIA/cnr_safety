
#include <moveit_msgs/GetPlanningScene.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_interface/planning_interface.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <configuration_msgs/StartConfiguration.h>
#include <subscription_notifier/subscription_notifier.h>
#include <sensor_msgs/JointState.h>
int main(int argc, char **argv)
{
  ros::init(argc,argv,"collision_supervisor");
  ros::NodeHandle nh;
  ros::AsyncSpinner spinner(4);
  spinner.start();
  std::string group_name="manipulator";

  ros::ServiceClient srv_client = nh.serviceClient<configuration_msgs::StartConfiguration>("/configuration_manager/start_configuration");
  configuration_msgs::StartConfiguration srv;
  srv.request.strictness = 1;
  srv.request.start_configuration="warmup1";

  robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
  robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
  planning_scene::PlanningScenePtr planning_scene = std::make_shared<planning_scene::PlanningScene>(kinematic_model);

  ros::ServiceClient ps_client=nh.serviceClient<moveit_msgs::GetPlanningScene>("/get_planning_scene");
  if (!ps_client.waitForExistence(ros::Duration(10)))
  {
    ROS_ERROR("unable to connect to /get_planning_scene");
    return 1;
  }

  ros_helper::SubscriptionNotifier<sensor_msgs::JointState> js_sub(nh,"/joint_states",1);
  ros_helper::SubscriptionNotifier<moveit_msgs::PlanningScene> ps_sub(nh,"/move_group/monitored_planning_scene",100);


  ROS_INFO("Waiting for a new message on topic /joint_states");
  if (!js_sub.waitForANewData(ros::Duration(5)))
  {
    ROS_INFO("Timeout expired, no new message received on topic /link/joint_states");
    return -1;
  }



  moveit_msgs::GetPlanningScene ps_srv;
  if (!ps_client.call(ps_srv))
  {
    ROS_ERROR("call to srv not ok");
    return 1;
  }


  if (!planning_scene->setPlanningSceneMsg(ps_srv.response.scene))
  {
    ROS_ERROR("unable to update planning scene");
    return 1;
  }

  ros::Rate lp(100);

  ros::WallTime t0=ros::WallTime::now();
  ros::WallDuration dt=ros::WallDuration(1);

  bool first_time=false;
  while(ros::ok())
  {
    if (ps_sub.isANewDataAvailable())
    {
      planning_scene->setPlanningSceneDiffMsg(ps_sub.getData());
      moveit_msgs::PlanningScene ps_msg;
      planning_scene->getPlanningSceneMsg(ps_msg);
    }

    if ((ros::WallTime::now()-t0)>dt)
    {
      t0=ros::WallTime::now();
      if (!ps_client.call(ps_srv))
      {
        ROS_ERROR("call to srv not ok");
      }
      if (!planning_scene->setPlanningSceneMsg(ps_srv.response.scene))
      {
        ROS_ERROR("unable to update planning scene");
      }
    }

    robot_state::RobotState state=planning_scene->getCurrentState();
    sensor_msgs::JointState js_msg=js_sub.getData();
    state.copyJointGroupPositions(group_name,js_msg.position);

    if (!state.satisfiesBounds())
    {
      ROS_WARN_THROTTLE(4,"Out of bound");
      srv_client.call(srv);
    }
    state.updateCollisionBodyTransforms();
    if (!planning_scene->isStateValid(state,group_name))
    {
      collision_detection::CollisionRequest col_req;
      collision_detection::CollisionResult col_res;
      col_req.contacts = true;
      col_req.group_name=group_name;
      planning_scene->checkCollision(col_req,col_res,state);
      if (first_time &&col_res.collision)
      {
        first_time=false;
        for (const  std::pair<std::pair<std::string, std::string>, std::vector<collision_detection::Contact> >& contact: col_res.contacts)
        {
         ROS_WARN("contact between %s and %s",contact.first.first.c_str(),contact.first.second.c_str());
        }
        srv_client.call(srv);
      }
    }
    else
      first_time=true;
    lp.sleep();
  }

  return 0;

}







