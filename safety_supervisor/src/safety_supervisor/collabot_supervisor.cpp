#include <safety_supervisor/collabot_supervisor.h>

std::vector<double> pos;
std::vector<double> target_pos;

void getJointStates(const sensor_msgs::JointStateConstPtr& msg)
{
  pos = msg.get()->position;
}

void getTargetJointStates(const sensor_msgs::JointStateConstPtr& msg)
{
  target_pos = msg.get()->position;
}

int main(int argc, char **argv)
{
  ros::init(argc,argv,"safety_supervisor");
  ros::NodeHandle nh;
  ROS_INFO("OUT");

//  ros::Subscriber sub_js = nh.subscribe("/link/joint_states",1,getJointStates);
//  ros::Subscriber sub_target_js = nh.subscribe("/target/joint_states",1,getTargetJointStates);

  ros::ServiceClient srv_client = nh.serviceClient<configuration_msgs::StopConfiguration>("/configuration_manager/stop_configuration");
  configuration_msgs::StopConfiguration srv_msg;
  srv_msg.request.strictness = 1;
  ros::Rate lp(100);
  ros::Publisher pub = nh.advertise<std_msgs::Int64>("/speed_ovr",1);
  std_msgs::Int64 scale_msg;
  scale_msg.data = 0;
  double scaling=100;

  ros_helper::SubscriptionNotifier<sensor_msgs::JointState> sub_js(nh,"/link/joint_states",1,boost::bind(&getJointStates,_1));
  ros_helper::SubscriptionNotifier<sensor_msgs::JointState> sub_target_js(nh,"/target/joint_states",1,boost::bind(&getTargetJointStates,_1));

  ROS_INFO("Waiting for a new message on topic /link/joint_states");
  if (!sub_js.waitForANewData(ros::Duration(5)))
  {
    ROS_INFO("Timeout expired, no new message received on topic /link/joint_states");
    return 0;
  }

  ROS_INFO("Waiting for a new message on topic /target/joint_states");
  if (!sub_target_js.waitForANewData(ros::Duration(5)))
  {
    ROS_INFO("Timeout expired, no new message received on topic /target/joint_states");
    return 0;
  }

  double err_pos;

  while(ros::ok())
  {
    ros::spinOnce();

    bool is_stopping=false;
    bool is_limit=false;
    for (unsigned int i=0;i<target_pos.size();i++)
    {
      err_pos = std::abs(target_pos.at(i) - pos.at(i));
      if (err_pos > 0.03)
      {
//        ROS_ERROR("Exceed maximum position from target. Stop current configuration");
        is_stopping=true;
      }
      else if (err_pos>0.015)
        is_limit=true;
    }
    if (is_stopping==true)
    {
      scaling = std::max(0.,scaling-2.);
    }
    else if (!is_limit)
    {
      scaling = std::min(100.,scaling+0.1);
    }

    scale_msg.data = scaling;
    pub.publish(scale_msg);
    lp.sleep();
    ros::spinOnce();
  }

  return 0;

}







