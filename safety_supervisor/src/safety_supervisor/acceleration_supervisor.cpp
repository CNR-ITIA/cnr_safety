#include <safety_supervisor/acceleration_supervisor.h>


namespace safety {

  AccelerationSupervisor::AccelerationSupervisor()
  {
    m_is_initialized = false;
  }

  void AccelerationSupervisor::init(std::vector<std::string>& joint_names)
  {
    m_joint_names = joint_names;

    //Topics
    m_sub_vel_ovr.reset(new ros_helper::SubscriptionNotifier<std_msgs::Float64>(m_nh,"/speed_ovr",1));
    m_sub_vel_ovr->setAdvancedCallback(boost::bind(&AccelerationSupervisor::velOverrideCallback,this,_1));
    m_sub_acc_ovr.reset(new ros_helper::SubscriptionNotifier<std_msgs::Float64>(m_nh,"/acceleration_ovr",1));
    m_sub_acc_ovr->setAdvancedCallback(boost::bind(&AccelerationSupervisor::accOverrideCallback,this,_1));

    //Params
    std::string robots_description;
    if (!m_nh.getParam("/robot_description", robots_description))
    {
      ROS_FATAL_STREAM("Parameter '/robot_description' does not exist");
      return;
    }
    m_robot_model = urdf::parseURDF(robots_description);

  
    m_nAx = joint_names.size();
    m_effort_limits.resize(m_nAx);
    m_velocity_limits.resize(m_nAx);
    m_acceleration_limits.resize(m_nAx);
    m_upper_limits.resize(m_nAx);
    m_lower_limits.resize(m_nAx);
    m_x.resize(m_nAx);
    m_Dx.resize(m_nAx);
    m_DDx.resize(m_nAx);
    m_DDx_sat.resize(m_nAx);
    m_Dx_sat.resize(m_nAx);
    m_vel_ratio = 1.0;
    m_acc_ratio = 1.0;

    for (unsigned int iAx=0; iAx<m_nAx; iAx++)
    {
      m_velocity_limits(iAx)= m_robot_model->getJoint(m_joint_names.at(iAx))->limits->velocity;
      m_effort_limits(iAx)= m_robot_model->getJoint(m_joint_names.at(iAx))->limits->effort;
      m_upper_limits(iAx) = m_robot_model->getJoint(m_joint_names.at(iAx))->limits->upper;
      m_lower_limits(iAx) = m_robot_model->getJoint(m_joint_names.at(iAx))->limits->lower;
      m_acceleration_limits(iAx)=m_velocity_limits(iAx)*10.0;


      if ((m_upper_limits(iAx)==0) && (m_lower_limits(iAx)==0))
      {
        m_upper_limits(iAx)=std::numeric_limits<double>::infinity();
        m_lower_limits(iAx)=-std::numeric_limits<double>::infinity();
        ROS_INFO("upper and lower limits are both equal to 0, set +/- infinity");
      }

      ///////
      //Moveit Overwrite-on-URDF Limits
      bool has_velocity_limits;
      if (!m_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/has_velocity_limits",has_velocity_limits))
        has_velocity_limits=false;
      bool has_acceleration_limits;
      if (!m_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/has_acceleration_limits",has_acceleration_limits))
        has_acceleration_limits=false;

      if (has_velocity_limits)
      {
        double vel;
        if (m_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_velocity",vel))
        {
          //Overwrite if limit is more strict
          if (vel<m_velocity_limits(iAx))
            m_velocity_limits(iAx)=vel;
        }
        else
          ROS_WARN("/robot_description_planning/joint_limits/%s/max_velocity is not defined",m_joint_names.at(iAx).c_str());
      }

      if (has_acceleration_limits)
      {
        double acc;
        if (m_nh.getParam("/robot_description_planning/joint_limits/"+m_joint_names.at(iAx)+"/max_acceleration",acc))
        {
          m_acceleration_limits(iAx)=acc;
        }
        else
          ROS_WARN("/robot_description_planning/joint_limits/%s/max_acceleration is not defined",m_joint_names.at(iAx).c_str());
      }

      ROS_INFO(" - Joint: %s",m_joint_names.at(iAx).c_str());
      ROS_INFO("position limits = [%f, %f]",m_lower_limits(iAx),m_upper_limits(iAx));
      ROS_INFO("velocity limits = [%f, %f]",-m_velocity_limits(iAx),m_velocity_limits(iAx));
      ROS_INFO("acceleration limits = [%f, %f]",-m_acceleration_limits(iAx),m_acceleration_limits(iAx));
    }

      m_is_initialized = true;
      ROS_INFO("Acceleration Supervisor correctly initialized!");
  }

  Eigen::VectorXd AccelerationSupervisor::getLimitedPos(Eigen::VectorXd x)
  { 
    if (!m_is_initialized)
    {
      ROS_ERROR("Unable to compute method. Initialize your class first using init() method.");
      return x;
    }
    
    m_x = x;
    for (unsigned int idx=0;idx<m_nAx;idx++)
      m_x(idx)=std::max(m_lower_limits(idx),std::min(m_upper_limits(idx),m_x(idx)));

    return m_x;   

  }

  Eigen::VectorXd AccelerationSupervisor::getLimitedVel(Eigen::VectorXd Dx)
  { 
    if (!m_is_initialized)
    {
      ROS_ERROR("Unable to compute method. Initialize your class first using init() method.");
      return Dx;
    }
    
    m_Dx = Dx;
    if (m_sub_acc_ovr->isANewDataAvailable())
    {
      m_vel_ratio = 1.0;
      ROS_WARN("Messages published on /acceleration_ovr topic. Unable to override velocity using /speed_ovr. Stop publishing on /acceleration_ovr first");
    }

      m_Dx_sat=m_Dx;
      for (unsigned int iAx=0; iAx<m_nAx; iAx++)
        m_vel_ratio=std::max(m_vel_ratio,std::abs(m_Dx(iAx))/m_velocity_limits(iAx));

      return m_Dx_sat*=m_vel_ratio;
    
  }


  Eigen::VectorXd AccelerationSupervisor::getLimitedAcc(Eigen::VectorXd DDx)
  { 
    if (!m_is_initialized)
    {
      ROS_ERROR("Unable to compute method. Initialize your class first using init() method.");
      return DDx;
    }
    
    m_DDx = DDx;
    if (m_sub_vel_ovr->isANewDataAvailable())
    {
      m_acc_ratio = 1.0;
      ROS_WARN("Messages published on /speed_ovr topic. Unable to override acceleration using /speed_ovr. Stop publishing on /speed_ovr first");
    }

    m_DDx_sat=m_DDx;
    for (unsigned int iAx=0; iAx<m_nAx; iAx++)
      m_acc_ratio=std::max(m_acc_ratio,std::abs(m_DDx(iAx))/m_acceleration_limits(iAx));

    return m_DDx_sat*=m_acc_ratio;
  }
  

  Eigen::VectorXd AccelerationSupervisor::getBreakingAcc(Eigen::VectorXd DDx, Eigen::VectorXd Dx, Eigen::VectorXd x)
  {
    if (!m_is_initialized)
    {
      ROS_ERROR("Unable to compute method. Initialize your class first using init() method.");
      return DDx;
    }
    
    m_x = x;
    m_Dx = Dx;
    m_DDx = DDx;

    //Computing breaking distance
    for (unsigned int iAx=0; iAx<m_nAx; iAx++)
    {
      double t_break=std::abs(m_Dx(iAx))/m_acceleration_limits(iAx);
      double breaking_distance=0.5*m_acceleration_limits(iAx)*std::pow(t_break,2.0);

      if (m_x(iAx) > (m_upper_limits(iAx)-breaking_distance))
      {
        if (m_Dx(iAx)>0)
        {
          ROS_WARN_THROTTLE(2,"Breaking, maximum limit approaching on joint %s",m_joint_names.at(iAx).c_str());
          m_DDx_sat(iAx)=-m_acceleration_limits(iAx);
        }
      }

      if (m_x(iAx) < (m_lower_limits(iAx) + breaking_distance))
      {
        if (m_Dx(iAx) < 0)
        {
          ROS_WARN_THROTTLE(2,"Breaking, minimum limit approaching on joint %s",m_joint_names.at(iAx).c_str());
          m_DDx_sat(iAx)=m_acceleration_limits(iAx);
        }
      }
    }
    return m_DDx_sat;
  }


  void AccelerationSupervisor::velOverrideCallback(const std_msgs::Float64ConstPtr& msg)
  {
    m_vel_ratio = msg->data;
  }


  void AccelerationSupervisor::accOverrideCallback(const std_msgs::Float64ConstPtr& msg)
  {
    m_acc_ratio = msg->data;
  }


    


}